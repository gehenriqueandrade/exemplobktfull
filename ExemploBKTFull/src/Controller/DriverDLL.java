package Controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

public class DriverDLL {
	private FuncoesDLL dll;
	Pointer pointer;
	int retorno;
	int p;
	byte[] n = new byte[] {};
	String retStatus;
	
	public DriverDLL() {
		try {
			dll = (FuncoesDLL) Native.loadLibrary("KIOSKDLL.dll", FuncoesDLL.class);
			System.out.println("DLL Carregada.");
		} catch (UnsatisfiedLinkError e) {
			System.out.println("Erro.");
			e.printStackTrace();
		}
	}
	public void AbrindoPortaDLL() {
		pointer = dll.KIOSK_OpenUsb();
	}
	public int MenuPrincipalUm() {
		System.out.println("     B     E     M             V     I     N     D     O     \n\n");
		System.out.println(" DESEJA UTILIZAR QUAL TIPO DE COMUNICA��O? ");
		System.out.println(" �   1   -   V I A   D L L ");
		System.out.println(" �   2   -   C O M A N D O   D I R E T O ");
		System.out.println(" �   3   -   F I N A L I Z A R   P R O G R A M A  ");
		
		Scanner teclado1 = new Scanner(System.in);
		int tecladoum = teclado1.nextInt();
		
		switch(tecladoum) {
		case 1: MenuPrincipalDLL();
			break;
		case 2: 
			break;
		case 3: 
			break;
		}
		
		return tecladoum;
	}
	public int MenuPrincipalDLL() {		
		System.out.println("--------------------------------------------------------");
		System.out.println("� M  E  N  U     P  R  I  N  C  I  P  A  L     D  L  L �");
		System.out.println("--------------------------------------------------------\n\n\n");
		System.out.println("�   [1]    IMPRESS�O CUPOM PADR�O - ELGIN.\n");
		System.out.println("�   [2]    STATUS DE IMPRESSORA, PAPEL E GAVETA.\n");
		System.out.println("�   [3]    IMPRESS�O CUPOM LAYOUT NOVO - ELGIN.\n");
		System.out.println("�   [4]    IMPRESS�O DE TODOS OS CODES\n");
		System.out.println("�   [5]    IMPRESS�O DE TODAS AS FONTES E TAMANHOS.\n");
		System.out.println("�   [6]    FINALIZAR.\n");
		
		Scanner teclado2 = new Scanner(System.in);
		int tecladodois = teclado2.nextInt();
		
		switch(tecladodois) {
		case 1: CupomPadraoDLL();
			break;
		case 2: StatusDLL();
			break;
		case 3: CupomLayoutNovoDLL();
			break;
		case 4: AllCodeDLL();
			break;
		case 5: AllFontDLL();
			break;
		case 6: retorno = dll.KIOSK_CloseUsb(p);
			break;
		}
		
		return tecladodois;
	}
	public void CupomPadraoDLL() {
		retorno = dll.KIOSK_S_SetAlignMode(pointer, 2, 1);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "Elgin Manaus\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "Elgin S/A\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "Rua Abiura, 579 Distrito Industrial Manaus - AM\n\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "CNPJ 14.200.166/0001-66  IE 111.111.111.111 IM\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "-----------------------------------------------------------\n", 0, 1, 1, 1, 8);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "Extrato No. 002046\n", 0, 1, 1, 1, 8);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "CUPOM FISCAL ELETRONICO - SAT\n", 0, 1, 1, 1, 8);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "-----------------------------------------------------------\n", 0, 1, 1, 1, 8);
		retorno = dll.KIOSK_S_SetAlignMode(pointer, 2, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "CPF/CNPJ Consumidor:\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_SetAlignMode(pointer, 2, 1);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "-----------------------------------------------------------\n", 0, 1, 1, 1, 8);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "#| COD | DESC | QTD | UN | VL UN R$ |(VL TR R$)*|VL ITEM R$\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "1 1000016 IMPRESSORA NAO FISCAL I9 3,449 U 2,89(3,84)100,00\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "Subtotal                                             100,00\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "Total R$                                             100,00\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "Dinheiro                                             100,00\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "Troco                                                  0,00\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "-----------------------------------------------------------\n", 0, 1, 1, 1, 8);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "Tributos Totais (Lei Fed. 12.741/12)R$                 3,85\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "-----------------------------------------------------------\n", 0, 1, 1, 1, 8);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "R$ 3,84 R$ 3,84 Trib Aprox R$ 1,35 Fed R$ 2,50 Est\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "Fonte: IBPT/FECOMERCIO(R$) 90i3aC\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "-----------------------------------------------------------\n", 0, 1, 1, 1, 8);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "* Valor aproximado dos tributos do item\n", 0, 1, 1, 1, 0);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "-----------------------------------------------------------\n", 0, 1, 1, 1, 8);
		retorno = dll.KIOSK_S_Textout(pointer, 2, "SAT No. 90000123\n", 0, 1, 1, 3, 8);
		Date agora = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String data2 = format.format(agora);
        retorno = dll.KIOSK_S_SetAlignMode(pointer, 2, 1);
        retorno = dll.KIOSK_S_Textout(pointer, 2, data2, 0, 1, 1, 3, 8);
        retorno = dll.KIOSK_FeedLines(pointer, 2, 2);
        retorno = dll.KIOSK_S_PrintBarcode(pointer, 2, "{B12345678901234567890", 10, 73, 2, 80, 0, 2, "{B12345678901234567890".length());
        retorno = dll.KIOSK_FeedLines(pointer, 2, 1);
        retorno = dll.KIOSK_CutPaper(pointer, 2, 0, 0);
	}
	public void StatusDLL() {
		retorno = dll.KIOSK_RTQueryStatusForT681(pointer, 2, retStatus);
		System.out.println(retorno);
	}
	public void CupomLayoutNovoDLL() {
		
	}
	public void AllCodeDLL() {
		
	}
	public void AllFontDLL() {
		
	}
}
