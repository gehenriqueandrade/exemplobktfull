package Controller;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

public interface FuncoesDLL extends Library {
	
	public int KIOSK_OpenCom(int IpName, int nComBaudrate, int nComDataBits, int nComStopBits, int nComParity, int nFlowControl);
	public int KIOSK_SetComTimeOuts(int hPort, int nWriteTimeoutMul, int nWriteTimeoutCon, int nReadTimeoutMul, int nReadTimeoutCon);
	public int KIOSK_CloseCom(int hPort);
	public int KIOSK_OpenLptByDrv(int LPTAddress);
	public int KIOSK_CloseDrvLPT(int nPortType);
	public Pointer KIOSK_OpenUsb();
	public int KIOSK_OpenUsbByID(int nID);
	public int KIOSK_SetUsbTimeOuts(int hPort, int wReadTimeouts, int wWriteTimeouts);
	public int KIOSK_CloseUsb(int hPort);
	public int KIOSK_OpenDrv(String drivername);
	public int KIOSK_StartDoc(int hPort);
	public int KIOSK_EndDoc(int hPort);
	public int KIOSK_CloseDrv(int hPort);
	public int KIOSK_WriteData(int hPort, int nPortType, String pszData, int nBytesToWrite);
	public int KIOSK_ReadData(int nPort, int nPortType, int nStatus, String pszBuffer, int nBytesToRead);
	public int KIOSK_SendFile(int nPort, int nPortType, String filename);
	public int KIOSK_BeginSaveToFile(int hPort, Pointer IpFileName, boolean bToPrinter);
	public int KIOSK_EndSaveToFile(int hPort);
	public int KIOSK_QueryStatus(int hPort, int nPortType, String pszStatus, int nTimeouts);
	public int KIOSK_RTQueryStatus(int hPort, int nPortType, String pszStatus);
	public int KIOSK_RTQueryStatusForT681(Pointer hPort, int nPortType, String pszStatus);
	public int KIOSK_QueryASB(int hPort, int nPortType, int Enable);
	public int KIOSK_SetASBForT681(int hPort, int nPortType, int Enable);
	public int KIOSK_GetVersionInfo(int pnMajor, int pnMinor);
	public int KIOSK_Reset(int hPort, int nPortType);
	public int KIOSK_SetPaperMode(int hPort, int nPortType, int nMode);
	public int KIOSK_SetMode(int hPort, int nPortType, int nPrintMode);
	public int KIOSK_SetMotionUnit(int hPort, int nPortType, int nHorizontalMU, int nVerticalMU);
	public int KIOSK_S_SetLeftMarginAndAreaWidth(int hPort, int nPortType, int nDistance, int nWidth);
	public int KIOSK_P_SetAreaAndDirection(int hPort, int nPortType, int nOrgx, int nOrgy, int nWidth, int nHeight, int nDirection);
	public int KIOSK_EnableMacro(int hPort, int nPortType);
	public int KIOSK_SetCharSetAndCodePage(int hPort, int nPortType, int nCharSet, int nCodePage);
	public int KIOSK_FontUDChar(int hPort, int nPortType, int nEnable, int DPI, int nChar, int nCharCode, String pCharBmpBuffer, int nDotsOfWidth, int nBytesOfHeight);
	public int KIOSK_SetOppositePosition(int hPort, int nPortType, int nPrintMode, int nHorizontalDist, int nVericalDist);
	public int KIOSK_SetTabs(int hPort, int nPortType, String pszPosition, int nCount);
	public int KIOSK_ExecuteTabs(int hPort, int nPortType);
	public int KIOSK_FeedLine(int hPort, int nPortType);
	public int KIOSK_FeedLines(Pointer hPort, int nPortType, int nLines);
	public int KIOSK_P_Print(int hPort, int nPortType);
	public int KIOSK_P_Clear(int hPort, int nPortType);
	public int KIOSK_CutPaper(Pointer hPort, int nPortType, int nMode, int nDistance);
	public int KIOSK_MarkerFeedPaper(int hPort, int nPortType);
	public int KIOSK_MarkerCutPaper(int hPort, int nPortType, int nDistance);
	public int KIOSK_SetLineSpacing(int hPort, int nPortType, int nDistance);
	public int KIOSK_SetRightSpacing(int hPort, int nPortType, int nDistance);
	public int KIOSK_S_SetAlignMode(Pointer hPort, int nPortType, int nMode);
	public int KIOSK_S_Textout(Pointer hPort, int nPortType, String pszData, int nOrgx, int nWidthTimes, int nHeightTimes, int nFontType, int nFontStyle);
	public int KIOSK_P_Textout(Pointer hPort, int nPortType, String pszData, int nOrgx, int nOrgy, int nWidthTimes, int nHeightTimes, int nFontType, int nFontStyle);
	public int KIOSK_S_PrintBarcode(Pointer hPort,
            						int nPortType,
						            String pszBuffer,
						            int nOrgx,
						            int nType,
						            int nWidth,
						            int nHeight,
						            int nHriFontType,
						            int nHriFontPosition,
						            int nBytesOfBuffer);
	public int KIOSK_P_PrintBarcode(int hPort, int nPortType, String pszBuffer, int nOrgx, int nOrgy, int nType, int nWidth, int nHeight, int nHriFontType, int nHriFontPosition, int nBytesOfBuffer);
	public int KIOSK_PreDownloadBmpToRAM(int hPort, int nPortType, String pszPaths, int nID);
	public int KIOSK_PreDownloadBmpToFlash(int hPort, int nPortType, String pszPaths[], int nCount);
	public int KIOSK_S_PrintBmpInRAM(int hPort, int nPortType, int nID, int nOrgx, int nMode);
	public int KIOSK_P_PrintBmpInRAM(int hPort, int nPortType, int nID, int nOrgx, int nOrgy, int nMode);
	public int KIOSK_S_PrintBmpInFlash(int hPort, int nPortType, int nID, int nOrgx, int nMode);
	public int KIOSK_P_PrintBmpInFlash(int  hPort, int nPortType, int nID, int nOrgx, int nOrgy, int nMode);
	public int KIOSK_S_DownloadPrintBmp(int hPort, int nPortType, String pszPath, int nOrgx, int nMode);
	public int KIOSK_P_DownloadPrintBmp(int hPort, int nPortType, String pszPath, int nOrgx, int nOrgy, int nMode);	
	public int KIOSK_S_TestPrint(int hPort, int nPortType);
	public int KIOSK_SetPrintFromStart(int hPort, int nPortType, int nMode);
	public int KIOSK_SetRaster(int hPort, int nPortType, String pszBmpPath, int nMode);
	public int KIOSK_CountMode(int hPort, int nPortType, int Enable, int nBits, int nOrder, int nSBound, int nEBound, int nTimeSpace, int nCycTime, int nCount);
	public int KIOSK_SetChineseFont(int hPort, int nPortType, String pszBuffer, int nEnable, int nBigger, int nLSpacing, int nRSpacing, int nUnderLine);
	public int KIOSK_P_Lineation(int hPort, int nPortType, int nWidth, int nSHCoordinate, int nSVCoordinate,int nEHCoordinate, int nEVCoordinate);
	public int KIOSK_BarcodeSetPDF417(int hPort, int nPortType, String pszBuffer, int nBytesOfBuffer, int nWidth, int nHeight, int nLines, int nColumns, int nScaleH, int nScaleV, int nCorrectGrade);
	public int KIOSK_SetPresenter(int hPort, int nPortType, int nMode, int nTime);
	public int KIOSK_SetBundler(int hPort, int nPortType, int nMode, int nTime);
	public int KIOSK_SetBundlerInfo(int hPort, int nPortType, int nMode);

}
